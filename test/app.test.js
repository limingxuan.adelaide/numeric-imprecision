const request = require('supertest');
const { app, getTaxedTotal } = require('./app');

describe('usability', () => {

    it('Requst for 500, should return 200', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: 500});
        expect(res.statusCode).toEqual(200);
        expect(res.text).toContain('550');
    });

    it('when request /status, should return 200', async () => {
        const res = await request(app)
            .get('/status');
        expect(res.statusCode).toEqual(200);
    });


});
