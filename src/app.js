'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main 
app.get('/', (req, res) => { 
    var { total, tax } = getTaxedTotal(req.query.amount);
    res.status(200).end('The total amount including tax is: ' + parseFloat(total))
});

// return total amount with tax
var getTaxedTotal = (value) => {
    var amount = parseFloat(value)
    if (amount < 0 || amount >= Number.MAX_SAFE_INTEGER ) {
        throw RangeError('amount is out of range')
    }
    var tax = amount * 0.10 //10% tax
    var total = amount + tax
    if (total >= Number.MAX_SAFE_INTEGER) {
        throw RangeError('total is out of range')
    }
    return { total: Number(total.toFixed(2)), tax: Number(tax.toFixed(2)) }
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, getTaxedTotal };
