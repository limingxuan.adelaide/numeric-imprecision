const request = require('supertest');
const { app, getTaxedTotal } = require('./app');

describe('security', () => {
    
    it('Request to -500, should throw RangeError exception', async () => {
        expect.assertions(1);
        try {
            getTaxedTotal("-500");
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it('1000000.1 amount, should return 1100000.11 total and 100000.01 tax', async () => {

        const { total, tax } = getTaxedTotal("1000000.1");
        expect(total).toBe(1100000.11);
        expect(tax).toBe(100000.01);
    });

    it('Number.MAX_SAFE_INTEGER+1 and Number.MAX_SAFE_INTEGER+2, should not return same total and tax and throw RangeError', async () => {
        expect.assertions(1);
        try {
            const { total: total1, tax: tax1 } = getTaxedTotal("9007199254740992");
            const { total: total2, tax: tax2 } = getTaxedTotal("9007199254740993");
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it('Number.MAX_SAFE_INTEGER-1, should not return same total and tax and throw RangeError', async () => {
        expect.assertions(1);
        try {
            const { total: total1, tax: tax1 } = getTaxedTotal("9007199254740990");
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

});
